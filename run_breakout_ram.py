# Script that runs Q-Learning on the version of Breakout that provides the
# RAM output as observations.

import gym
import numpy as np
import pickle
import random
import os.path
import sys
import time

# function to convert observation to string
def obsToString(obs):
    obsStr = ""
    for val in obs:
        obsStr += str(val) + ","
    obsStr = obsStr[:-1]
    return obsStr

# function to convert observation to score
# blocks are 8 x 6 pixels
def obsToScore(obs):
    blockArea = obs[57:93][:,8:152]
    score = 0
    for i in range(6):
        for j in range(18):
            if j == 17:
                score += int(sum(sum(sum(blockArea[6*i:6*i+6][:,8*j:8*j+7]))) == 0)
            else:
                score += int(sum(sum(sum(blockArea[6*i:6*i+6][:,8*j:8*j+8]))) == 0)
    return score

# epsilon-greedy strategy
def epsilonGreedy(s, epsilon, Q, actions = range(0,4)):
    if s not in Q.keys():
        a = random.choice(actions)
        return a
    
    if random.random() < epsilon:
        a = random.choice(actions)
    else:
        qMax = -100000000
        for act, q in Q[s].items():
            if q > qMax:
                a = int(act)
                qMax = q
    return a


def main():
    
    # track runtime +  scores
    startTime = time.time()

    if len(sys.argv) != 5:
        raise Exception("Usage: python run_breakout_ram.py <train/test> <numIters> <epsilon> <Q-score file>")

    # only do rendering if testing
    if sys.argv[1] not in ['train', 'test']:
        raise Exception("Error: Must select train or test")
    else:
        trainTest = sys.argv[1]

    # number of iterations
    if int(sys.argv[2]) > 0:
        numIters = int(sys.argv[2])
    else:
        raise Exception("Error: Number of iterations must be a positive integer")

    # epsilon
    if float(sys.argv[3]) < 0 or float(sys.argv[3]) > 1:
        raise Exception("Error: Epsilon must be between 0 and 1")
    else:
        epsilon = float(sys.argv[3])

    # Q-score file
    Qfile = sys.argv[4]

    # track scores for testing
    if trainTest == 'test':
        scores = []
        score = 0

    env = gym.make('Breakout-ram-v0')
    env.reset()

    # read in existing Q file
    if os.path.isfile(Qfile):
        Q = pickle.load(open(Qfile,"rb"))
        print "Q table read in from " + Qfile
    else:
        Q = {}
        print "Initializing Q table..."

    # initial state + action
    # only render if testing
    #if trainTest == 'test':
        #env.render()
    # take a random action
    if trainTest == 'test':
        print "Starting testing..."
    else:
        print "Starting training..."
    act = env.action_space.sample()
    o, r, done, info = env.step(act)
    s = obsToString(o)
    a = str(act)
    lives = info['ale.lives']

    # SARSA
    alpha = 0.1
    gamma = 0.95
    for i in range(numIters):
        if (i > 0) and (i % 1000 == 0):
            print str(i) + " iterations complete"
        # only render if testing
        #if trainTest == 'test':
         #   env.render()
        # nextAction = env.action_space.sample()
        nextAction = epsilonGreedy(s, epsilon, Q)
        o, r, done, info = env.step(nextAction) 
        score += r
        #print score
        sp = obsToString(o)
        ap = str(nextAction)
        livesp = info['ale.lives']
        #print lives

        # figure out where it dies
        if (lives == 0):
            # compute score for testing
            if trainTest == 'test':
                scores.append(score)
                score = 0
            #print "Score: " + str(score)
            env.reset()
            act = env.action_space.sample()
            o, r, done, info = env.step(act)
            score += r
            s = obsToString(o)
            a = str(act)
            continue

        if s not in Q.keys():
            # initialize Q[s,a] to 0
            Q[s] = {}
            for pa in range(0,4):
                Q[s][str(pa)] = 0
        if sp not in Q.keys():
            # initialize Q[s,a] to 0
            Q[sp] = {}
            for pa in range(0,4):
                Q[sp][str(pa)] = 0
        Q[s][a] += alpha * (r + gamma * Q[sp][ap] - Q[s][a])
        s = sp
        a = ap
        lives = livesp


    # close game
    print str(numIters) + " iterations complete!"
    print str(len(Q.keys())) + " unique states seen"
    env.render(close=True)
    # write out Q table to file
    pickle.dump(Q, open( Qfile, "wb" ))

    # measure runtime
    endTime = time.time()
    elapsedTime = endTime - startTime
    print "Total runtime: " + '%.2f' % (elapsedTime / 60) + " min"

    # print score statistics if testing
    if trainTest == 'test':
        print "Games completed: " + str(len(scores))
        print "Max score: " + str(max(scores))
        print "Average score: " + '%.2f' % np.mean(scores)


if __name__ == '__main__':
    main()

