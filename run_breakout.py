# Script that runs a very simple version of Breakout.

import gym

num_episodes = 1
num_steps = 1000

env = gym.make('Breakout-v0')

for episode in range(num_episodes):
    observation = env.reset()
    for t in range(num_steps):
        env.render()
        print(observation)

        # Takes a random action
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
    
        if done:
            print("Episode finished after {} timesteps".format(t + 1))
