# Script that runs Q-learning on the Breakout game that provides screen pixels
# as observations.
#
# Breaks up the screen between the paddle and the bricks into patches of
# different sizes in order to reduce the size of the state space.

import argparse
from collections import deque
import gym
import numpy as np
import os.path
import pickle
import random
import sys
import tensorflow as tf
import time



#tf.logging.set_verbosity(tf.logging.INFO)

def main():
    env = gym.make('Breakout-v0')
    args = parse_arguments()
    print(args)

    # Track running time
    start_time = time.time()

    # Run Deep Q-Learning
    complete_scores, incomplete_scores = q_learn(env,
            num_episodes=args.episodes,
            num_time_steps=args.time_steps,
            replay_size=args.replay_size,
            gamma=args.gamma,
            epsilon=args.epsilon,
            minibatch_size=args.minibatch_size,
            render=args.render,
            enable_frame_skipping=(not args.noframe_skipping))

    # Print summary
    print('Finished running Deep Q-Learning')
    print('\tTotal runtime: %.2f sec' % (time.time() - start_time))
    print('\tEpisodes: %d' % args.episodes)
    print('\tTime steps per episode: %d' % args.time_steps)

    # Print score statistics
    print_score_stats(complete_scores, incomplete_scores)

    # Clean up the display window if necessary
    if args.render: env.render(close=True)


def print_score_stats(complete_scores, incomplete_scores):
    print('Games completed: %d' % len(complete_scores))
    print('\tMax score: %s' % (str(max(complete_scores)) if complete_scores \
        else 'n/a'))
    print('\tAverage score: %.2f' % (np.mean(complete_scores)
            if complete_scores else 0))
    print('Games terminated: %d' % len(incomplete_scores))
    print('\tMax score: %s' % (
            str(max(incomplete_scores)) if incomplete_scores else 'n/a'))
    print('\tAverage score: %.2f' % (np.mean(incomplete_scores)
            if incomplete_scores else 0))


def parse_arguments():
    parser = argparse.ArgumentParser(
            description='Run Deep Q-Learning on Atari Breakout')
    parser.add_argument('--epsilon', metavar='E', type=float,
            help='epsilon value for epsilon-greedy', default = 0.1)
    parser.add_argument('--render', action='store_true',
            help='Whether to render the environment on the screen.')
    parser.add_argument('--episodes', metavar='M', type=int,
            help='How many episodes to run', default=1)
    parser.add_argument('--time_steps', metavar='T', type=int,
            help='How many time steps to run in each episode', default=10)
    parser.add_argument('--gamma', metavar='G', type=float,
            help='Q-learning discount factor', default=0.95)
    parser.add_argument('--minibatch_size', metavar='J', type=int,
            help='Number of samples in minibatch', default=32)
    parser.add_argument('--noframe_skipping', action='store_true',
            help='Whether to disable frame skipping', default=False)
    parser.add_argument('--replay_size', metavar='N', type=int,
            help='Size of replay memory', default=10)
    return parser.parse_args()


def q_learn(env, num_episodes=1, num_time_steps=1000, replay_size=1, gamma=1.0,
        epsilon=1.0, minibatch_size=1, render=True, enable_frame_skipping=True):

    # Initialize replay memory D to capacity N
    replay_memory = deque(maxlen=replay_size)
    image_sequence = deque(maxlen=4)
    for i in range(4):
        image_sequence.append(np.zeros((210, 160, 3)))

    # Initialize action-value function Q with random weights
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(
            tensors=tensors_to_log, every_n_iter=50)
    classifier = tf.estimator.Estimator(
            model_fn=create_q_model, model_dir='/tmp/q_learning_model')

    complete_scores = []
    incomplete_scores = []
    total_time_steps = 0

    for episode in range(num_episodes):
        # Reset the environment to a new game.
        env.reset()
        if render: env.render()
        score = 0

        # Initialize sequence s1 = {x1} and preprocessed sequence
        # phi1 = phi(s1)
        a_t = env.action_space.sample()
        obs, reward, done, info = env.step(a_t) 
        score += reward
        if render: env.render()

        # Store four images in the sequence.
        image_sequence.append(obs)
        phi_t = preprocess(image_sequence)

        # Take an initial training step on this first example.
        train_model(classifier, logging_hook, [phi_t], [reward])

        for t in range(num_time_steps):
            # Print score stats
            if t % 100 == 0:
                print("Reached Episode %d, Time step %d" % (episode, t))
                print_score_stats(complete_scores, incomplete_scores)

            # Skip the action selection if this frame should be skipped.
            if not enable_frame_skipping or t % 4 == 0:
                a_t = select_action(env, classifier, logging_hook, phi_t,
                        epsilon=get_annealed_epsilon(epsilon, t))

            # Execute action at in emulator and observe reward rt and image xt+1
            x_t1, r_t, done, info = env.step(a_t) 
            score += r_t

            # print "Observation:", x_t1.shape, sum(sum(sum(x_t1)))
            print("Reward: %.2f, \tDone: %r, \tInfo: %s" % (
                r_t, done, str(info)))

            # If the game is complete, record score and terminate this episode.
            if done:
                break

            # Render the new state if desired
            if render: env.render()

            # Skip the update if this frame should be skipped.
            if enable_frame_skipping and t % 4 != 0:
                continue

            # Set st+1 = st, at, xt+1 and preprocess phi_t1 = phi(s_t1)
            image_sequence.append(x_t1)
            phi_t1 = preprocess(image_sequence)

            # Store transition (phi_t, a_t, r_t, phi_t1) in D
            replay_memory.append((phi_t, a_t, r_t, phi_t1))
            phi_t = phi_t1

            # Sample random minibatch of transitions (phi_j , aj , rj , phi_j1)
            # from D
            sample_size = min(minibatch_size, len(replay_memory))
            minibatch = random.sample(replay_memory, sample_size)

            # Train on the minibatch
            minibatch_start = time.time()
            phi_array = []
            y_array = []
            for example in minibatch:
                phi_j, a_j, r_j, phi_j1 = example
                if phi_j1 is None:
                    y_j = r_j
                else:
                    max_q = get_best_q_value(classifier, logging_hook, phi_j1)
                    y_j = r_j + gamma * max_q
                phi_array.append(phi_j)
                y_array.append(y_j)

            # Perform a gradient descent step on (y_j - Q(phi_j, a_j))**2
            train_model(classifier, logging_hook, phi_array, y_array)
            print('Minibatch processing took %.2f seconds' % (
                time.time() - minibatch_start))

        # Record the score at the end of this episode.
        if done:
            complete_scores.append(score)
        else:
            incomplete_scores.append(score)
        total_time_steps += t

        print("Finished Episode %d at time step %d" % (episode, t))
        print_score_stats(complete_scores, incomplete_scores)
        print("Total time steps: %d" % total_time_steps)

    return (complete_scores, incomplete_scores)


def get_annealed_epsilon(epsilon, t):
    """Linearly anneals epsilon from 1.0 to the value of epsilon over 1,000,000
       frames."""
    annealing_time_steps = float(1000000)
    annealed_epsilon = epsilon + (1.0 - epsilon) * \
            max(annealing_time_steps - t, 0)/annealing_time_steps
    return annealed_epsilon



# Based on https://www.tensorflow.org/tutorials/layers
# and https://www.tensorflow.org/get_started/estimator
def create_q_model(features, labels, mode, params):
    """Creates a deep convolutional neural network based on the dsecription
       given in the DeepMind Deep Q-Learning paper."""

    # Convert to grayscale
    grayscale_images = tf.image.rgb_to_grayscale(features['x'],
            name="grayscale")
    grayscale_images = tf.reshape(grayscale_images, [-1, 210, 160, 4])

    # Print a tensor
    t = tf.Print(grayscale_images, [grayscale_images])
    result = t + 1

    # Downsample to (110, 84)
    downsampled_images = tf.image.resize_images(
            grayscale_images, size=[110, 84])

    # Crop to (84, 84)
    cropped_images = tf.image.crop_to_bounding_box(
        downsampled_images,
        offset_height=(110-84),
        offset_width=0,
        target_height=84,
        target_width=84)

    # Input layer: 84 x 84 x 4 image produced by preprocess() method
    input_layer = tf.reshape(cropped_images, [-1, 84, 84, 4],
            name='reshape_features')

    # First hidden layer: convolves 16 8 x 8 filters with stride 4 
    # with the input image and applies a rectifier nonlinearity
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=16,
        kernel_size=[8, 8],
        strides=(4, 4),
        name='conv1',
        activation=tf.nn.relu)

    # Second hidden layer: convolves 32 4 x 4 filters with stride 2,
    # followed by a rectifier nonlinearity.
    conv2 = tf.layers.conv2d(
        inputs=conv1,
        filters=32,
        kernel_size=[4, 4],
        strides=(2, 2),
        name='conv2',
        activation=tf.nn.relu)

    # Final hidden layer: fully-connected and consists of 256 rectifier units
    conv2_flat = tf.reshape(conv2, [-1, 9 * 9 * 32], name='hidden_reshape')
    dense = tf.layers.dense(inputs=conv2_flat, units=256, activation=tf.nn.relu,
            name='dense')

    # Output layer: fully connected linear layer with single output per action
    output = tf.layers.dense(inputs=dense, units=4, name='output')

    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "classes": tf.argmax(input=output, axis=1),
        # Add `softmax_tensor` to the graph. It is used by the `logging_hook`.
        "probabilities": tf.nn.softmax(output, name="softmax_tensor"),
        'q_values': output
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.losses.mean_squared_error(labels=labels, predictions=output)

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.RMSPropOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(
                loss=loss,
                global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss,
                train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def select_action(env, classifier, logging_hook, phi_t, epsilon=0.1):
    """Select an action using the epsilon-greedy strategy."""
    # If less than epsilon, explore an action.
    if random.random() < epsilon:
        next_action = env.action_space.sample()

    # Otherwise, select the action that gives the highest Q-value.
    # Select a_t = max_a Q*(phi_t, a)
    else:
        next_action = get_best_action(classifier, logging_hook, phi_t)

    return next_action


def preprocess(sequence):
    """Stacks the last four frames into a numpy array"""
    return np.array(sequence)


def train_model(classifier, logging_hook, phi, y_values):
    """Train the classifier on this example."""
    # Broadcast the labels in numpy to get the right shape for loss calcs.
    # TODO: Do this in tensorflow instead.
    labels = np.reshape(np.repeat(y_values, 4), (len(y_values), 4))

    train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": np.array(phi)},
            y=labels,
            num_epochs=1,
            shuffle=True)
    classifier.train(input_fn=train_input_fn, steps=1, hooks=[logging_hook])


def get_best_q_value(classifier, logging_hook, phi):
    prediction = predict(classifier, logging_hook, phi)
    return max(prediction['q_values'])


def get_best_action(classifier, logging_hook, phi):
    """Return the class of the best prediction for this example."""
    prediction = predict(classifier, logging_hook, phi)
    return prediction['classes']


def predict(classifier, logging_hook, phi):
    """Predict the Q value using the given classifier and phi."""
    input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": np.array([phi])},
            num_epochs=1,
            shuffle=False)
    prediction = list(
            classifier.predict(input_fn=input_fn, hooks=[logging_hook]))[0]
    return prediction


def calculate_score(image):
    """Calculates the score of the game from an image of the screen. Blocks are
       8 x 6 pixels."""
    blockArea = image[57:93][:,8:152]
    score = 0
    for i in range(6):
        for j in range(18):
            if j == 17:
                score += int(sum(sum(sum(
                    blockArea[6*i:6*i+6][:,8*j:8*j+7]))) == 0)
            else:
                score += int(sum(sum(sum(
                    blockArea[6*i:6*i+6][:,8*j:8*j+8]))) == 0)
    return score


if __name__ == '__main__':
    main()
