# Script that runs Q-learning on the Breakout game that provides screen pixels
# as observations.
#
# Breaks up the screen between the paddle and the bricks into patches of
# different sizes in order to reduce the size of the state space.

import gym
import numpy as np
import pickle
import random
import os.path
import sys
import time

# function to convert observation to string
def obsToString(obs):
    oSub = obs[93:193][:,8:152]
    oStr = ""
    
    # 16 x 16 patches at the top
    for i in range(3):
        for j in range(9):
            # last column of boxes has one less pixel per row
            if j == 8:
                oStr += str(int(sum(sum(sum(oSub[16*i:16*i+16][:,16*j:16*j+15]))) > 0)) + ","
            else:
                oStr += str(int(sum(sum(sum(oSub[16*i:16*i+16][:,16*j:16*j+16]))) > 0)) + ","
        
    # 8 x 8 patches in middle 
    for i in range(3):
        for j in range(18):
            # last column of boxes has one less pixel per row
            if j == 17:
                oStr += str(int(sum(sum(sum(oSub[48+8*i:48+8*i+8][:,8*j:8*j+7]))) > 0)) + ","
            else:
                oStr += str(int(sum(sum(sum(oSub[48+8*i:48+8*i+8][:,8*j:8*j+8]))) > 0)) + ","

    # 4 x 4 patches on bottom 
    for i in range(7):
        for j in range(36):
            # last column of boxes has one less pixel per row
            if j == 35:
                oStr += str(int(sum(sum(sum(oSub[72+4*i:72+4*i+4][:,4*j:4*j+3]))) > 0)) + ","
            else:
                oStr += str(int(sum(sum(sum(oSub[72+4*i:72+4*i+4][:,4*j:4*j+4]))) > 0)) + ","

    # bottom area with the paddle
    for j in range(9):
        # last column of boxes has one less pixel per row
        if j == 8:
            oStr += str(int(sum(sum(sum(oSub[96:100][:,16*j:16*j+15]))) > 0)) + ","
        else:
            oStr += str(int(sum(sum(sum(oSub[96:100][:,16*j:16*j+16]))) > 0)) + ","

    return oStr[:-1]

# function to convert observation to score
# blocks are 8 x 6 pixels
def obsToScore(obs):
    blockArea = obs[57:93][:,8:152]
    score = 0
    for i in range(6):
        for j in range(18):
            if j == 17:
                score += int(sum(sum(sum(blockArea[6*i:6*i+6][:,8*j:8*j+7]))) == 0)
            else:
                score += int(sum(sum(sum(blockArea[6*i:6*i+6][:,8*j:8*j+8]))) == 0)
    return score

# epsilon-greedy strategy
def epsilonGreedy(s, epsilon, Q, actions = range(0,4)):
    if s not in Q.keys():
        a = random.choice(actions)
        return a
    
    if random.random() < epsilon:
        a = random.choice(actions)
    else:
        qMax = -100000000
        for act, q in Q[s].items():
            if q > qMax:
                a = int(act)
                qMax = q
    return a


def main():

    # track runtime +  scores
    startTime = time.time()

    if len(sys.argv) != 5:
        raise Exception("Usage: python run_breakout_squares.py <train/test> <numIters> <epsilon> <Q-score file>")

    # only do rendering if testing
    if sys.argv[1] not in ['train', 'test']:
        raise Exception("Error: Must select train or test")
    else:
        trainTest = sys.argv[1]

    # number of iterations
    if int(sys.argv[2]) > 0:
        numIters = int(sys.argv[2])
    else:
        raise Exception("Error: Number of iterations must be a positive integer")

    # epsilon
    if float(sys.argv[3]) < 0 or float(sys.argv[3]) > 1:
        raise Exception("Error: Epsilon must be between 0 and 1")
    else:
        epsilon = float(sys.argv[3])

    # Q-score file
    Qfile = sys.argv[4]

    # track scores for testing
    if trainTest == 'test':
        scores = []

    env = gym.make('Breakout-v0')
    env.reset()

    # read in existing Q file
    if os.path.isfile(Qfile):
        Q = pickle.load(open(Qfile,"rb"))
        print "Q table read in from " + Qfile
    else:
        Q = {}
        print "Initializing Q table..."

    # initial state + action
    # only render if testing
    #if trainTest == 'test':
        #env.render()
    # take a random action
    if trainTest == 'test':
        print "Starting testing..."
    else:
        print "Starting training..."
    act = env.action_space.sample()
    o, r, done, info = env.step(act)
    s = obsToString(o)
    a = str(act)
    lives = info['ale.lives']

    # SARSA
    alpha = 0.1
    gamma = 0.95
    for i in range(numIters):
        # update on progress
        if (i > 0) and (i % 1000 == 0):
            print str(i) + " iterations complete"
        # only render if testing
        #if trainTest == 'test':
            #env.render()
        # select action + observe reward + next state
        nextAction = epsilonGreedy(s, epsilon, Q)
        o, r, done, info = env.step(nextAction) 
        sp = obsToString(o)
        ap = str(nextAction)
        livesp = info['ale.lives']
        # add penalty for dying
        if (livesp < lives):
            r -= 5

        # figure out where it dies (runs out of lives)
        if (lives == 0):
            # compute score for testing
            if trainTest == 'test':
                score = obsToScore(o)
                scores.append(score)
            #print "Score: " + str(score)
            env.reset()
            act = env.action_space.sample()
            o, r, done, info = env.step(act)
            s = obsToString(o)
            a = str(act)
            lives = info['ale.lives']
            continue

        if s not in Q.keys():
            # initialize Q[s,a] to 0
            Q[s] = {}
            for pa in range(0,4):
                Q[s][str(pa)] = 0
        if sp not in Q.keys():
            # initialize Q[s,a] to 0
            Q[sp] = {}
            for pa in range(0,4):
                Q[sp][str(pa)] = 0

        Q[s][a] += alpha * (r + gamma * Q[sp][ap] - Q[s][a])
        s = sp
        a = ap
        lives = livesp
    
    # close game
    print str(numIters) + " iterations complete!"
    print str(len(Q.keys())) + " unique states seen"
    env.render(close=True)
    # write out Q table to file
    pickle.dump(Q, open( Qfile, "wb" ))

    # measure runtime
    endTime = time.time()
    elapsedTime = endTime - startTime
    print "Total runtime: " + '%.2f' % (elapsedTime / 60) + " min"

    # print score statistics if testing
    if trainTest == 'test':
        print "Games completed: " + str(len(scores))
        print "Max score: " + str(max(scores))
        print "Average score: " + '%.2f' % np.mean(scores)


if __name__ == '__main__':
    main()
