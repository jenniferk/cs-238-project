# README #

This repository contains the code for our final project for CS 238.

Docs for setting up OpenAI Gym:  
https://gym.openai.com/docs/

To get set up:  

1. Clone this repository.
2. In the directory above this one, run  
   git clone https://github.com/openai/gym  
   cd gym  
   pip install -e . # minimal install  
   pip install -e '.[atari]'  
3. cd into this directory and run python run_breakout.py.
